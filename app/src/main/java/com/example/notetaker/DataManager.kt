package com.example.notetaker

object DataManager {
    val courses = HashMap<String, CourseInfo>()
    val notes = ArrayList<NoteInfo>()

    init {
        initializeCourses()
        initializeNotes()
    }

    private fun initializeCourses() {
        var course = CourseInfo("android_intents", "Android Programming with Intents")
        courses.set(course.courseId, course)

        course = CourseInfo("Android_async", "Android async programming and services")
        courses.set(course.courseId, course)

        course = CourseInfo("Java Fundamentals: The Java Language", "Java_lang")
        courses.set(course.courseId, course)

        course = CourseInfo("Java_core", "Java Fundamentals: Core java")
        courses.set(course.courseId, course)

    }

    private fun initializeNotes() {

    }
}